from behave import *
import time
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

# username=""
# password=""

@given(u'the user has the correct credentials')
def step_impl(context):
    context.browser.get('https://sprinkle-burn.glitch.me/')



@when(u'the user enters email as "{login_email}"')
def step_impl(context,login_email):
    context.browser.find_element_by_name("email").send_keys(login_email)



@when(u'the user enters password as "{password}"')
def step_impl(context,password):
    context.browser.find_element_by_name("password").send_keys(password)


@when(u'clicks Login')
def step_impl(context):
    context.browser.find_element_by_css_selector(".f5").click()
    time.sleep(1)

@then(u'the user is presented with a welcome message')
def step_impl(context):
   assert context.browser.find_element_by_xpath("//article").text == 'Welcome Dr I Test'

@given(u'the user has the incorrect credentials')
def step_impl(context):
    context.browser.get('https://sprinkle-burn.glitch.me/')



@then(u'the user is presented with a error message')
def step_impl(context):
    assert context.browser.find_element_by_id("login-error-box").text == 'Credentials are incorrect'


@then(u'the user is presented with a "{error_message}"')
def step_impl(context,error_message):
   # TODO: Get the Tooltip element locator from development team and verify
   # assert context.browser.find_element_by_id("tooltip_id").text== error_message
    print(error_message)

