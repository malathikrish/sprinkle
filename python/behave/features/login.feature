Feature: Login
  In order to use the app the user must be able to Login

  @smoke
  Scenario: Login Success
    Given the user has the correct credentials
    When the user enters email as "test@drugdev.com"
    And the user enters password as "supers3cret"
    And clicks Login
    Then the user is presented with a welcome message

  @regression
  Scenario: Login Incorrect credentials
    Given the user has the incorrect credentials
    When the user enters email as "test@drugdev.com"
    And the user enters password as "wrong_password"
    And clicks Login
    Then the user is presented with a error message

  @regression @negative
  Scenario Outline: eMail field validation
    Given the user has the correct credentials
    When the user enters email as "<test_email>"
    And clicks Login
    Then the user is presented with a "<error_message>"

    Examples:
      |test_email  |error_message|
      | user       | Please include '@' in the email address. '@' is missing in 'user' is missing an '@'|
      | user@      | Please enter a part folllowing '@'. user@ is incomplete|
      | user@@doamin.com| A part following @ shouldnot conatain the symbol '@'|
      | user@doamin.    | '.' is used at a wrong position in 'domain..'    |